﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using RateMeDemo.Model;

namespace RateMeDemo.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        // GET: api/Auth
        /// <summary>
        /// AuthController GET: api/Auth
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("login")]
        public IActionResult Login(User user)
        {
            if (user == null)
            {
                return BadRequest("Invalid client request");
            }

            if (user.Username == "sandra" && user.Password == "sani123")
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:5000",
                    audience: "http://localhost:5000",
                    claims: new List<Claim>(),
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

                JwtHeader header = new JwtHeader();
                header.Add("Content-Type", "application/json");
                header.Add("token", tokenString);

                //string withHeader = 
                  //  header.ToString() + " " + tokenString;

                //return Ok(new { Token = withHeader });
                return Ok(header);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
