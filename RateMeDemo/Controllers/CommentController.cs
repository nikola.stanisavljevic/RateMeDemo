﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RateMeDemo.DataProvider;
using RateMeDemo.Model;

namespace RateMeDemo.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        /// <summary>
        /// CommentController GET: api/Comment
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Comment> Get()
        {
            List<Comment> comm = new List<Comment>();
            comm = CommentDataProvider.GetComments();
            return comm;
        }


        /// <summary>
        /// CommentController GET: api/Comment/5
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetComment")]
        public Comment Get(int id)
        {
            Comment c = new Comment();
            c = CommentDataProvider.GetComment(id);
            return c;
        }

        /// <summary>
        /// CommentController POST: api/Comment
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public void Post(Comment c)
        {
            CommentDataProvider.AddComment(c);
        }

        /// <summary>
        /// CommentController PUT: api/Comment/5
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public void Put(int id, Comment c)
        {
            CommentDataProvider.UpdateComment(id, c);
        }

        /// <summary>
        /// CommentController DELETE: api/Comment/5
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            CommentDataProvider.DeleteComment(id);
        }
    }
}
