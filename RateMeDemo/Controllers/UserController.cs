﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RateMeDemo.Model;

namespace RateMeDemo.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
            /// <summary>
            /// UserController GET: api/User
            /// </summary>
            /// <returns></returns>
            [HttpGet]
            public IEnumerable<User> Get()
            {
                List<User> users = new List<User>();
                users = UserDataProvider.GetUsers();
                return users;
            }

            /// <summary>
            /// UserController GET: api/User/5
            /// </summary>
            /// <returns></returns>
            [HttpGet("{id}", Name = "GetUser")]
            public User Get(int id)
            {
                 User u = new User();
                 u = UserDataProvider.GetUser(id);
                 return u;
            }

            /// <summary>
            /// UserController POST: api/User
            /// </summary>
            /// <returns></returns>
            [HttpPost]
            public void Post(User u)
            {
                UserDataProvider.AddUser(u);
            }

            /// <summary>
            /// UserController PUT: api/User/5
            /// </summary>
            /// <returns></returns>
            [HttpPut("{id}")]
            public void Put(int id, User u)
            {
                UserDataProvider.UpdateUser(id, u);
            }

            /// <summary>
            /// UserController DELETE: api/User/5
            /// </summary>
            /// <returns></returns>
            [HttpDelete("{id}")]
            public void Delete(int id)
            {
                UserDataProvider.DeleteUser(id);
            }
    }
}