﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RateMeDemo.DataProvider;
using RateMeDemo.Model;


namespace RateMeDemo.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class ObjectInstanceController : ControllerBase
    {
        /// <summary>
        /// ObjectInstanceController GET: api/ObjectInstance
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JArray Get()
        {
            JArray ar = new JArray();
            List<Dictionary<string, string>> dictionary = new List<Dictionary<string, string>>();
            dictionary = ObjectInstanceDataProvider.GetObjectInstances();

            for(int i=0;i<dictionary.Count;i++)
            {
                Dictionary<string, string> dictionary_element = new Dictionary<string, string>();
                dictionary_element = dictionary.ElementAt(i);
                JObject jobj = new JObject();

                for (int j=0; j<dictionary_element.Count;j++)
                {
                    jobj.Add(dictionary_element.ElementAt(j).Key, dictionary_element.ElementAt(j).Value);


                }
                ar.Add(jobj);
            }
            return ar;
        }

        /// <summary>
        /// ObjectInstanceController GET: api/ObjectInstance/5
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetObject")]
        public JObject Get(int id)
        {
            ObjectInstance u = DataProvider.ObjectInstanceDataProvider.GetObjectInstance(id);
            JObject j = new JObject();
            for (int i = 0; i < u.ObjectAttributes.Count; i++)
                j.Add(u.ObjectAttributes.ElementAt(i).Key,u.ObjectAttributes.ElementAt(i).Value);
            return j;
        }

        /// <summary>
        /// ObjectInstanceController GET: api/CategoryObject/"type"
        /// Where type is a string value of Category.Name (ex. firma,park,restoran)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{CategoryObject}/{type?}")]
        public List<Dictionary<string, string>> GetCategoryObject(string type)
        {
            List<Dictionary<string, string>> obList = new List<Dictionary<string, string>>();
            obList = ObjectInstanceDataProvider.GetCategoryObjects(type);
            return obList;
        }

        /// <summary>
        /// ObjectInstanceController POST: api/ObjectInstance
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public void Post([FromBody] IDictionary<string,string> value)
        {
            DataProvider.ObjectInstanceDataProvider.AddObjectInstance(value);
        }

        /// <summary>
        /// ObjectInstanceController PUT: api/ObjectInstance/5
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]  IDictionary<string, string> value)
        {
            DataProvider.ObjectInstanceDataProvider.UpdateObjectInstance(id,value);
        }

        /// <summary>
        /// ObjectInstanceController DELETE: api/ObjectInstance/5
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            DataProvider.ObjectInstanceDataProvider.DeleteObjectInstance(id);
        }
    }
}
