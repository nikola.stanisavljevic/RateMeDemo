﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RateMeDemo.DataProvider;
using RateMeDemo.Model;

namespace RateMeDemo.Controllers
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        /// <summary>
        /// CategoryController GET: api/Category
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Category> Get()
        {
            List<Category> cat = new List<Category>();
            cat = CategoryDataProvider.GetCategories();
            return cat;
        }

        /// <summary>
        /// CategoryController GET: api/Category/5
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetCategory")]
        public Category Get(int id)
        {
            Category u = new Category();
            u = DataProvider.CategoryDataProvider.GetCategory(id);
            return u;
        }

        /// <summary>
        /// CategoryController POST: api/Category
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public void Post(Category u)
        {
            CategoryDataProvider.AddCategory(u);
        }

        /// <summary>
        /// CategoryController PUT: api/Category/5
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public void Put(int id, Category u)
        {
            CategoryDataProvider.UpdateCategory(id, u);
        }

        /// <summary>
        /// CategoryController DELETE: api/Category/5
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
           CategoryDataProvider.DeleteCategory(id);
        }
    }
}