﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RateMeDemo.DataProvider;
using RateMeDemo.Model;
using System.Collections.Generic;

namespace RateMeDemo.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        /// <summary>
        /// RatingController GET: api/Rating
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Rating> Get()
        {
            List<Rating> cat = new List<Rating>();
            cat = RatingDataProvider.GetRatings();
            return cat;
        }

        /// <summary>
        /// RatingController GET: api/Rating/5
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetRating")]
        public Rating Get(int id)
        {
            Rating u = new Rating();
            u = DataProvider.RatingDataProvider.GetRating(id);
            return u;
        }

        /// <summary>
        /// RatingController POST: api/Rating
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public void Post(Rating u)
        {
            RatingDataProvider.AddRating(u);

        }

        /// <summary>
        /// RatingController PUT: api/Rating/5
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public void Put(int id, Rating value)
        {
            RatingDataProvider.UpdateRating(id,value);
        }

        /// <summary>
        /// RatingController DELETE: api/Rating/5
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            RatingDataProvider.DeleteRating(id);
        }
    }
}
