﻿using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo
{
    public class ConnectDb
    {
        public static GraphClient client;
      
        /// <summary>
        /// Connection via GraphClient to Neo4j DB remote
        /// </summary>
        /// <returns></returns>
        public static void Connect()
        {
            if (client == null)
            {
                //client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "123"); //local

                client = new GraphClient(new Uri("http://192.168.0.242:7474/db/data"), "neo4j", "joka");
                try
                {
                    client.Connect();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
            }
        }
    }
}
