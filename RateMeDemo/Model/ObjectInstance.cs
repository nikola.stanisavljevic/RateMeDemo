﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    /// <summary>
    /// Model ObjectInstance
    /// Attributes: IdObject, Type, ObjectAttributes
    /// Methods: MappAttributes()
    /// </summary>
    /// <returns></returns>
    public class ObjectInstance
    {
        public int IdObject { get; set; }
        public string Type { get; set; }
        public Dictionary<string, string> ObjectAttributes { get; set; }

        public ObjectInstance() {   }

        public void MapAttributes()
        {
            this.Type = ObjectAttributes.GetValueOrDefault("type");
        }
    }
}