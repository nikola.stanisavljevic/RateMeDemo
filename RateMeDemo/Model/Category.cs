﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{ 
    /// <summary>
    /// Model Category
    /// Attributes: IdCategory, Name, IdUser, DescriptionAttributes, RatingAttributes
    /// </summary>
    /// <returns></returns>
    public class Category
    {
        public int  IdCategory {get; set;}
        public string Name {get; set; }
        public int IdUser { get; set; }
        public List<string> DescriptionAttributes { get; set; }
        public List<string> RatingAttributes { get; set; }

        public Category() { }
    }
}
