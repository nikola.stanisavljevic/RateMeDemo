﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    /// <summary>
    /// Model User
    /// Attributes: IdUser, Type, Username, Email, Password
    /// </summary>
    /// <returns></returns>
    public class User
    {
        public int IdUser { get; set; }
        public Usertype Type { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public User() { }
    }
}
