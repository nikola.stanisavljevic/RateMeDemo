﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    /// <summary>
    /// Model Rating
    /// Attributes: IdObjectInstance, IdUser, RatingValues
    /// </summary>
    /// <returns></returns>
    public class Rating
    {
        public int IdObjectInstance { get; set; }
        public int IdUser { get; set; }
        public List< int> RatingValues { get; set; }

        public Rating() { }
    }
}
