﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    /// <summary>
    /// Model Comment
    /// Attributes: IdUser, IdObjectInstance, CommentText
    /// </summary>
    /// <returns></returns>
    public class Comment
    {
        public int IdUser { get; set; }
        public int IdObjectInstance { get; set; }
        public String CommentText { get; set; }

        public Comment() { }
    }
}
