﻿using Neo4jClient;
using Neo4jClient.Cypher;
using RateMeDemo.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo
{
    public class UserDataProvider
    {

        #region GET USER
        /// <summary>
        /// Retrieves the a User with specified id
        /// MATCH(n) WHERE ID(n) = 2 RETURN n
        /// </summary>
        /// <returns></returns>
        internal static User GetUser(int id)
        {
            User u = new User();
          
            ConnectDb.Connect();
            
            var query = ConnectDb.client.Cypher.Match("(n:User)").Where("ID(n) = {id}").
                WithParam("id", id).Return((n) => n.As<User>()).Results.ToList();

            if (query.Count != 0)
            {
                u = query[0];
                return u;
            }
            else return null;
        }

        /// <summary>
        /// Retrieves all Users from DB
        /// MATCH (n:User) RETURN n
        /// </summary>
        /// <returns></returns>
        internal static List<User> GetUsers()
        { 
            List<User> users = new List<User>();

            ConnectDb.Connect();

            return users = ConnectDb.client.Cypher.Match("(n:User)")
               .Return((n) => n.As<User>()).Results.ToList();
        }
        #endregion

        #region POST USER
        /// <summary>
        /// Creates a new User in DB
        /// CREATE (n:User {password:'1234', username:'NewUser', email:'new@usermail.com'})
        /// </summary>
        /// <returns></returns>
        internal static void AddUser(User u)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
               .Create("(n:User {u})")
               .WithParam("u", u)
               .ExecuteWithoutResults();
        }
        #endregion

        #region PUT USER
        /// <summary>
        /// Updates an existing User in DB
        /// MATCH (u:User) WHERE ID(u) = 1 SET u.username = 'newUsername'
        /// MATCH (u:User) WHERE ID(u) = 7 SET u = { username: 'Michael' }
        /// </summary>
        /// <returns></returns>
        internal static void UpdateUser(int id, User u)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>
            { { "id", id }, { "u", u } };
            ConnectDb.Connect();
            ConnectDb.client.Cypher
                .Match("(n:User)")
                .Where("ID(n) = {id}")
                .Set("n = {u}")
                .WithParams(dict)
                .ExecuteWithoutResults();
        }
        #endregion

        #region DELETE USER
        /// <summary>
        /// Deletes an existing User in DB
        /// MATCH(n: Person { name: 'UNKNOWN' }) DELETE n
        /// </summary>
        /// <returns></returns>
        internal static void DeleteUser(int id)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
              .OptionalMatch("(n:User)")
              .Where("ID(n) = {id}")
              .WithParam("id", id)
              .Delete("n")
              .ExecuteWithoutResults();
        }
        #endregion        
    }
}
