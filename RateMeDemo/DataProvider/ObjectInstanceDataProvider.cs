﻿using RateMeDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.DataProvider
{
    public class ObjectInstanceDataProvider
    {
        #region GET OBJECT INSTANCE
        /// <summary>
        /// ObjectInstanceDataProvider
        /// Retrieves the a Object Instance with specified id
        /// MATCH(n:ObjectInstance) WHERE ID(n) = 2 RETURN n
        /// </summary>
        /// <returns></returns>
        internal static ObjectInstance GetObjectInstance(int id)
        {
            ObjectInstance u = new ObjectInstance();

            ConnectDb.Connect();

            var query = ConnectDb.client.Cypher.Match("(n)").Where("ID(n) = {id}").
                WithParam("id", id).Return((n) => n.As<Dictionary<string,string>>()).Results.ToList();


            if (query.Count != 0)
            {
                u.ObjectAttributes = query[0];
                return u;
            }
            else
                return null;
        }

        /// <summary>
        /// Retrieves all Object Instances from DB
        /// MATCH (n:ObjectInstance) RETURN n
        /// </summary>
        /// <returns></returns>
        internal static List<Dictionary<string,string>> GetObjectInstances()
        {
            ConnectDb.Connect();
        
            return   ConnectDb.client.Cypher.Match("(n)")
               .Return((n) => n.As<Dictionary<string, string>>()).Results.ToList();
        }

        /// <summary>
        /// Retrieves all Object Instances of a certain category Type from DB
        /// MATCH(n:ObjectInstance) WHERE n.Type = 'NEKITIP' RETURN n
        /// </summary>
        /// <returns></returns>
        internal static List<Dictionary<string, string>> GetCategoryObjects(string type)
        {
            List<ObjectInstance> obList = new List<ObjectInstance>();
            ConnectDb.Connect();

            return ConnectDb.client.Cypher.Match("(n)").Where("n.Type = {type}").
                WithParam("type", type).Return((n) => n.As<Dictionary<string, string>>()).Results.ToList();
        }
        #endregion

        #region POST OBJECT INSTANCE
        /// <summary>
        /// Creates a new Object Instance in DB
        /// CREATE (n:ObjectInstance {attribute1:'value1', ...'})
        /// </summary>
        /// <returns></returns>
        internal static void AddObjectInstance(IDictionary<string,string> u)
        {
               ConnectDb.Connect();
               IEnumerable<int> id=  ConnectDb.client.Cypher
                                 .Create("(n)")
                                 .Return<int>("ID(n)").Results;
            int id_1 = id.ElementAt(0);

            for (int i = 0; i < u.Count; i++)
                ConnectDb.client.Cypher.Match("(n)").Where("ID(n)="+id_1).                    
                    Set("n."+u.ElementAt(i).Key+"= '"+u.ElementAt(i).Value+"'").ExecuteWithoutResults();

        }
        #endregion

        #region PUT OBJECT INSTANCE
        /// <summary>
        /// Updates an existing Object Instance in DB
        /// MATCH (u:ObjectInstance) WHERE ID(u) = 1 SET u.Attribute1 = 'newValue1'
        /// </summary>
        /// <returns></returns>
        internal static void UpdateObjectInstance(int id, IDictionary<string, string> u)
        {
            ConnectDb.Connect();

            for (int i = 0; i < u.Count; i++)
                ConnectDb.client.Cypher.Match("(n)").Where("ID(n)=" + id).
                    Set("n." + u.ElementAt(i).Key + "= '" + u.ElementAt(i).Value + "'").ExecuteWithoutResults();
        }
        #endregion

        #region DELETE OBJECT INSTANCE
        /// <summary>
        /// Deletes an existing Object Instancw in DB
        /// MATCH(n:ObjectInstance) WHERE ID(n) = 5 DELETE n
        /// </summary>
        /// <returns></returns>
        internal static void DeleteObjectInstance(int id)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
              .OptionalMatch("(n)")
              .Where("ID(n) = {id}")
              .WithParam("id", id)
              .Delete("n")
              .ExecuteWithoutResults();
        }
        #endregion  
    }
}
