﻿using RateMeDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.DataProvider
{
    public class CommentDataProvider
    {

        #region GET COMMENT
        /// <summary>
        /// CommentDataProvider
        /// MATCH(n:Comment) WHERE ID(n) = 2 RETURN n
        /// </summary>
        /// <returns></returns>
        internal static Comment GetComment(int id)
        {
            Comment c = new Comment();
            ConnectDb.Connect();

            var query = ConnectDb.client.Cypher.Match("(n:Comment)").Where("ID(n) = {id}").
                WithParam("id", id).Return((n) => n.As<Comment>()).Results.ToList();

            if (query.Count != 0)
            {
                c = query[0];
                return c;
            }
            else return null;
        }

        /// <summary>
        /// Retrieves all Comments from DB
        /// MATCH (n:Comment) RETURN n
        /// </summary>
        /// <returns></returns>
        internal static List<Comment> GetComments()
        {
            List<Comment> comms = new List<Comment>();
            ConnectDb.Connect();

            return comms = ConnectDb.client.Cypher.Match("(n:Comment)")
               .Return((n) => n.As<Comment>()).Results.ToList();
        }
        #endregion

        #region POST COMMENT
        /// <summary>
        /// Creates a new Comment in DB
        /// CREATE (n:Comment {IdUser:123, IdObejctInstance:321, CommentText:'Ovo je komentar.'})
        /// </summary>
        /// <returns></returns>
        internal static void AddComment(Comment c)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
               .Create("(n:Comment {c})")
               .WithParam("c", c)
               .ExecuteWithoutResults();
        }
        #endregion

        #region PUT COMMENT
        /// <summary>
        /// Updates an existing Comment in DB
        /// MATCH (n:Comment) WHERE ID(n) = 5 SET n.Attribute1 = 'newValue1'
        /// </summary>
        /// <returns></returns>
        internal static void UpdateComment(int id, Comment c)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>
            { { "id", id }, { "c", c } };
            ConnectDb.Connect();
            ConnectDb.client.Cypher
                .Match("(n:Comment)")
                .Where("ID(n) = {id}")
                .Set("n = {c}")
                .WithParams(dict)
                .ExecuteWithoutResults();
        }
        #endregion

        #region DELETE COMMENT
        /// <summary>
        /// Deletes an existing Comment in DB
        /// DELETE (n:Comment) WHERE ID(n) = 5
        /// </summary>
        /// <returns></returns>
        internal static void DeleteComment(int id)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
              .OptionalMatch("(n:Comment)")
              .Where("ID(n) = {id}")
              .WithParam("id", id)
              .Delete("n")
              .ExecuteWithoutResults();
        }
        #endregion        
    }
}
