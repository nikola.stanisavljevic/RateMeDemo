﻿using RateMeDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.DataProvider
{
    public class RatingDataProvider
    {
        #region GET RATING
        /// <summary>
        /// RatingDataProvider
        /// MATCH(n:Rating) WHERE ID(n) = 2 RETURN n
        /// </summary>
        /// <returns></returns>
        internal static Rating GetRating(int id)
        {
            Rating c = new Rating();
            ConnectDb.Connect();

            var query = ConnectDb.client.Cypher.Match("(n:Rating)").Where("ID(n) = {id}").
                WithParam("id", id).Return((n) => n.As<Rating>()).Results.ToList();

            if (query.Count != 0)
            {
                c = query[0];
                return c;
            }
            else return null;

        }

        /// <summary>
        /// RatingDataProvider
        /// MATCH(n:Rating) RETURN n
        /// </summary>
        /// <returns></returns>
        internal static List<Rating> GetRatings()
        {
            List<Rating> ratings = new List<Rating>();

            ConnectDb.Connect();

            return ratings = ConnectDb.client.Cypher.Match("(n:Rating)")
               .Return((n) => n.As<Rating>()).Results.ToList();
        }
        #endregion

        #region POST RATING
        /// <summary>
        /// RatingDataProvider
        /// CREATE(n:Rating {attibute1 : 'value1', ...}) 
        /// </summary>
        /// <returns></returns>
        internal static void AddRating(Rating u)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
               .Create("(n:Rating {c})")
               .WithParam("c", u)
               .ExecuteWithoutResults();
        }
        #endregion

        #region PUT RATING
        /// <summary>
        /// RatingDataProvider
        /// MATCH(n:Rating) WHERE ID(n) = 2 SET n.Attribute1 = 'newValue1'
        /// </summary>
        /// <returns></returns>
        internal static void UpdateRating(int id, Rating c)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("id", id);
            dict.Add("c", c);
            ConnectDb.Connect();
            ConnectDb.client.Cypher
                .Match("(n:Rating)")
                .Where("ID(n) = {id}")
                .Set("n = {c}")
                .WithParams(dict)
                .ExecuteWithoutResults();
        }
        #endregion

        #region DELETE RATING
        /// <summary>
        /// RatingDataProvider
        /// MATCH(n:Rating) WHERE ID(n) = 2 DELETE n
        /// </summary>
        /// <returns></returns>
        internal static void DeleteRating(int id)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
              .OptionalMatch("(n:Rating)")
              .Where("ID(n) = {id}")
              .WithParam("id", id)
              .Delete("n")
              .ExecuteWithoutResults();
        }
        #endregion
    }
}
