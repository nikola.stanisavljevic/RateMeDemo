﻿using RateMeDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.DataProvider
{
    public class CategoryDataProvider
    {
        #region GET CATEGORY
        /// <summary>
        /// CategoryDataProvider 
        /// MATCH (n:Category) WHERE ID(n) = 5 RETURN n
        /// </summary>
        /// <returns></returns>
        internal static Category GetCategory(int id)
        {
            Category c = new Category();
            ConnectDb.Connect();

            var query = ConnectDb.client.Cypher.Match("(n:Category)").Where("ID(n) = {id}").
                WithParam("id", id).Return((n) => n.As<Category>()).Results.ToList();

            if (query.Count != 0)
            {
                c = query[0];
                return c;
            }
            else return null;

        }

        /// <summary>
        /// CategoryDataProvider 
        /// MATCH (n:Category) RETURN n 
        /// </summary>
        /// <returns></returns>
        internal static List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();

            ConnectDb.Connect();

            return categories = ConnectDb.client.Cypher.Match("(n:Category)")
               .Return((n) => n.As<Category>()).Results.ToList();
        }
        #endregion

        #region POST CATEGORY
        /// <summary>
        /// CategoryDataProvider 
        /// CREATE (n:Category { attribute1 : 'value1', ...})
        /// </summary>
        /// <returns></returns>
        internal static void AddCategory(Category u)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
               .Create("(n:Category {c})")
               .WithParam("c", u)
               .ExecuteWithoutResults();

            //MATCH(n:Category) SET n.IdCategory = ID(n)
            ConnectDb.client.Cypher
               .Match("(n:Category)")
               .Set("n.IdCategory = ID(n)")
               .ExecuteWithoutResults();
        }
        #endregion

        #region PUT CATEGORY
        /// <summary>
        /// CategoryDataProvider 
        /// MATCH (n:Category) WHERE ID(n) = 5 SET n.Attribute1 = 'newValue' 
        /// </summary>
        /// <returns></returns>
        internal static void UpdateCategory(int id, Category c)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("id", id);
            dict.Add("c", c);
            ConnectDb.Connect();
            ConnectDb.client.Cypher
                .Match("(n:Category)")
                .Where("ID(n) = {id}")
                .Set("n = {c}")
                .WithParams(dict)
                .ExecuteWithoutResults();
        }
        #endregion

        #region DELETE CATEGORY
        /// <summary>
        /// CategoryDataProvider 
        /// DELETE (n:Category) WHERE ID(n) = 5
        /// </summary>
        /// <returns></returns>
        internal static void DeleteCategory(int id)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
              .OptionalMatch("(n:Category)")
              .Where("ID(n) = {id}")
              .WithParam("id", id)
              .Delete("n")
              .ExecuteWithoutResults();
        }
        #endregion
    }
}
